FROM node:10.16-alpine
LABEL maintainer="Alexander Moreno Delgado <alexander.moreno@0x365.com>"

ENV STORE_URL <MISSING STORE_URL>
ENV API_TOKEN <MISSING API_TOKEN>
ENV PORT 3000

RUN apk --update --no-cache add \
    curl \
    bash \
    git \
    python2

COPY scripts /usr/bin/

RUN chmod +x /usr/bin/entrypoint.sh 

RUN touch ~/.bashrc
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
RUN source ~/.bashrc
RUN npm config set user 0 \
    && npm config set unsafe-perm true \
    && npm install -g @bigcommerce/stencil-cli \
    && npm install -g webpack-merge \
    && npm link webpack

ENTRYPOINT ["/usr/bin/entrypoint.sh"]