#!/usr/bin/env sh

# Stencil Init and push a stencil theme to BigCommerce
# Required environment variables:
#   STORE_URL
#   API_TOKEN

if [[ ${DEBUG} == true ]]; then
  export
fi

npm config set user 0
npm config set unsafe-perm true
npm install
yes "" | stencil init --url ${STORE_URL} --token ${API_TOKEN} --port ${PORT}
stencil-push --activate
