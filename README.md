# stencil-cli-docker
This image allow execute stencil-cli in a tiny Docker image powered by Alpine Linux
stencil cli provide a way to initialize .stencil file and upload a file, the main purpose of this container is be used to CI/CD pipelines.

# How to use this image

Basic local execution:

```bash
docker run --rm us.gcr.io/x365-pipes/tools/stencil-cli -e API_TOKEN=API_TOKEN_VALUE -e STORE_URL=STORE_URL_VALUE
```

# Variables

You have pass the following variables:

| Variable | Value |
| --- | --- |
| STORE_URL | The BigCommerce storefront URL |
| API_TOKEN | The BigCommerce API Token |